Source: guayadeque
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>
Build-Depends:
 cmake (>= 2.6),
 debhelper-compat (= 9),
 gettext,
 libcurl4-gnutls-dev (>= 7.18.2),
 libdbus-1-dev,
 libgpod-dev [!hurd-any],
 libgstreamer0.10-dev,
 libindicate-dev (>= 0.6.92),
 libsqlite3-dev,
 libtagc0-dev,
 libwxgtk3.0-dev,
 libwxsqlite3-3.0-dev
Standards-Version: 3.9.5
Homepage: http://sourceforge.net/projects/guayadeque
Vcs-Git: https://salsa.debian.org/multimedia-team/guayadeque.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/guayadeque

Package: guayadeque
Architecture: any
Depends:
 gstreamer0.10-plugins-base,
 gstreamer0.10-plugins-good,
 ${misc:Depends},
 ${shlibs:Depends}
Description: lightweight music player
 Guayadeque is a lightweight and easy-to-use music player that supports
 smart playlists and huge music collections.
 .
 Main features include:
  - Play mp3, ogg, flac, wma, mpc, mp4, ape, etc.
  - Read and write tags in all supported formats.
  - Allow to catalogue your music using labels. Any track, artist or album
    can have as many labels you want.
  - Smart play mode that add tracks that fit your music taste using the
    tracks currently in play list.
  - Ability to download covers manually or automatically
  - Suggest music using last.fm service.
  - Allow fast access to any music file by genre, artist, album, etc
  - Play shoutcast radios.
  - Allow to subscribe to podcasts and download all new episodes
    automatically or manually.
  - Dynamic or static play lists.
  - Tracks tag editor with automatically fetching of tags information for
    easily completion.
  - Lyrics downloads from different lyrics providers.
  - Easily expandable contextual links support. With it you can find
    information about a track, an artist or an album on your favourite site.
  - Easily expandable contextual commands support. For example you can right
    click on any album and click in option to record the album in a burning
    application.
  - Option to copy the selection you want to a directory or device using a
    configurable pattern.
  - Last.fm audioscrobbling support.
  - Partial GNOME session support to detect when GNOME session is about to
    close and save the play list so it can continue next time with the same
    tracks.
  - Allow to resume play status and position when closed and reopened.
  - You can rate the tracks from 0 to 5 stars.
  - MPRIS D-Bus interface support so it can easily controlled from music
    applets for example and many more.

Package: guayadeque-dbg
Section: debug
Architecture: any
Depends:
 guayadeque (= ${binary:Version}),
 ${misc:Depends}
Description: lightweight music player - debugging symbols
 Guayadeque is a lightweight and easy-to-use music player that supports
 smart playlists and huge music collections.
 .
 This package provides the debugging symbols for guayadeque.
